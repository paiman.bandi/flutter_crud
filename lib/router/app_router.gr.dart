// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i3;
import 'package:flutter/material.dart' as _i4;
import 'package:flutter_crud/presentation/screens/movie_crud_screen.dart'
    as _i1;
import 'package:flutter_crud/presentation/screens/movie_main_screen.dart'
    as _i2;

abstract class $AppRouter extends _i3.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i3.PageFactory> pagesMap = {
    MovieCrudRoute.name: (routeData) {
      final args = routeData.argsAs<MovieCrudRouteArgs>(
          orElse: () => const MovieCrudRouteArgs());
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.MovieCrudScreen(
          key: args.key,
          movieId: args.movieId,
        ),
      );
    },
    MovieMainRoute.name: (routeData) {
      return _i3.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.MovieMainScreen(),
      );
    },
  };
}

/// generated route for
/// [_i1.MovieCrudScreen]
class MovieCrudRoute extends _i3.PageRouteInfo<MovieCrudRouteArgs> {
  MovieCrudRoute({
    _i4.Key? key,
    String? movieId,
    List<_i3.PageRouteInfo>? children,
  }) : super(
          MovieCrudRoute.name,
          args: MovieCrudRouteArgs(
            key: key,
            movieId: movieId,
          ),
          initialChildren: children,
        );

  static const String name = 'MovieCrudRoute';

  static const _i3.PageInfo<MovieCrudRouteArgs> page =
      _i3.PageInfo<MovieCrudRouteArgs>(name);
}

class MovieCrudRouteArgs {
  const MovieCrudRouteArgs({
    this.key,
    this.movieId,
  });

  final _i4.Key? key;

  final String? movieId;

  @override
  String toString() {
    return 'MovieCrudRouteArgs{key: $key, movieId: $movieId}';
  }
}

/// generated route for
/// [_i2.MovieMainScreen]
class MovieMainRoute extends _i3.PageRouteInfo<void> {
  const MovieMainRoute({List<_i3.PageRouteInfo>? children})
      : super(
          MovieMainRoute.name,
          initialChildren: children,
        );

  static const String name = 'MovieMainRoute';

  static const _i3.PageInfo<void> page = _i3.PageInfo<void>(name);
}
