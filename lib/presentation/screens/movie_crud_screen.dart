import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud/core/service_locator.dart';
import 'package:flutter_crud/data/movie_data.dart';
import 'package:flutter_crud/presentation/stores/del_btn_store.dart';
import 'package:flutter_crud/presentation/stores/movie_store.dart';
import 'package:flutter_crud/router/app_router.gr.dart';
import 'package:flutter_crud/util/popup_util.dart';
import 'package:flutter_crud/util/uuid_util.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

@RoutePage()
class MovieCrudScreen extends StatelessWidget {
  final String? movieId;

  MovieCrudScreen({super.key, this.movieId});

  final _formKey = GlobalKey<FormState>();
  final TextEditingController titleController = TextEditingController();
  final TextEditingController directorController = TextEditingController();
  final TextEditingController summaryController = TextEditingController();

  final List<String> genres = [
    "Drama",
    "Action",
    "Animation",
    "Sci-Fi",
    "Horror",
  ];

  void _toggleSelection(String genre) {
    final MovieStore movieStore = getIt.get<MovieStore>();

    if (movieStore.selectedGenres.contains(genre)) {
      movieStore.removeGenre(genre);
    } else {
      movieStore.addGenre(genre);
    }
  }

  Widget _wInputMovie() {
    final DelBtnStore delBtnStore = getIt.get<DelBtnStore>();
    final MovieStore movieStore = getIt.get<MovieStore>();

// Check if movieId is not null and a movie with this ID exists
    Movie? movie;
    if (movieId != null) {
      movie = movieStore.findMovieById(movieId!);
    }

    // Set the text for the controllers
    titleController.text =
        delBtnStore.isDelBtnShown && movie != null ? movie.title : '';
    directorController.text =
        delBtnStore.isDelBtnShown && movie != null ? movie.director : '';
    summaryController.text =
        delBtnStore.isDelBtnShown && movie != null ? movie.summary : '';

    return Form(
        key: _formKey,
        child: Column(children: [
          Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: TextFormField(
                controller: titleController,
                decoration: InputDecoration(
                  labelText: 'Title',
                  hintText: 'Enter Title',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 12.0,
                    horizontal: 16.0,
                  ),
                ),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return 'Please enter the title!';
                  }
                  return null;
                },
              )),
          Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: TextFormField(
                controller: directorController,
                decoration: InputDecoration(
                  labelText: 'Director',
                  hintText: 'Enter Director',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 12.0,
                    horizontal: 16.0,
                  ),
                ),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return 'Please enter the director!';
                  }
                  return null;
                },
              )),
          Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              child: TextFormField(
                maxLines: 3,
                maxLength: 100,
                keyboardType: TextInputType.multiline,
                controller: summaryController,
                decoration: InputDecoration(
                  labelText: 'Summary',
                  hintText: 'Enter Summary',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  contentPadding: const EdgeInsets.symmetric(
                    vertical: 12.0,
                    horizontal: 16.0,
                  ),
                ),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return 'Please enter the summary!';
                  }
                  return null;
                },
              )),
        ]));
  }

  Widget _wSelectGenre(String? movieId) {
    final DelBtnStore delBtnStore = getIt.get<DelBtnStore>();
    final MovieStore movieStore = getIt.get<MovieStore>();
    return Column(
      children: <Widget>[
        Wrap(
          spacing: 8.0,
          runSpacing: 4.0,
          children: genres.map((genre) {
            return Observer(
                builder: (_) => ElevatedButton(
                      onPressed: () {
                        if (delBtnStore.isDelBtnShown) {
                          movieStore.setIsUpdatedGenre(true);
                        }
                        _toggleSelection(genre);
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: movieId != null &&
                                delBtnStore.isDelBtnShown &&
                                movieStore.isUpdatedGenre == false &&
                                movieStore
                                        .findMovieById(movieId)
                                        ?.genres
                                        .contains(genre) ==
                                    true
                            ? Colors.deepPurple
                            : (movieStore.selectedGenres.contains(genre)
                                ? Colors.deepPurple
                                : Colors.grey),
                        foregroundColor: Colors.white,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 8.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                      child: Text(
                        genre,
                        style: const TextStyle(fontSize: 16.0),
                      ),
                    ));
          }).toList(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final DelBtnStore delBtnStore = getIt.get<DelBtnStore>();
    final MovieStore movieStore = getIt.get<MovieStore>();
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              movieStore.setIsUpdatedGenre(false);
              movieStore.clearSelectedGenres();
              AutoRouter.of(context).push(const MovieMainRoute());
            },
          ),
          actions: <Widget>[
            Observer(
              builder: (_) => Visibility(
                  visible: delBtnStore.isDelBtnShown,
                  child: ElevatedButton.icon(
                    icon: const Icon(Icons.delete), // Icon
                    label: const Text('Delete'), // Label
                    onPressed: () {
                      movieStore.deleteMovie(movieId!);
                      AutoRouter.of(context).push(const MovieMainRoute());
                    },
                  )),
            ),
            ElevatedButton.icon(
              icon: const Icon(Icons.save), // Icon
              label: const Text('Save'), // Label
              onPressed: () {
                final movie = Movie(
                    id: movieId,
                    title: titleController.text,
                    director: directorController.text,
                    summary: summaryController.text,
                    genres: movieStore.selectedGenres.toList());
                if (_formKey.currentState != null &&
                    _formKey.currentState!.validate()) {
                  if (delBtnStore.isDelBtnShown) {
                    movieStore.updateMovie(movieId!, movie);
                    movieStore.setIsUpdatedGenre(false);
                    displaySuccessPopup(context, 'Movie updated successfully.');
                  } else {
                    movie.id = generateUUID();
                    movieStore.addMovie(movie);
                    displaySuccessPopup(context, 'Movie added successfully.');
                    _formKey.currentState!.reset();
                    movieStore.clearSelectedGenres();
                  }
                }
              },
            ),
          ],
        ),
        body: Column(
          children: [_wInputMovie(), _wSelectGenre(movieId)],
        ));
  }
}
