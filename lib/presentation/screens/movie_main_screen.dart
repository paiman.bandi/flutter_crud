import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud/core/service_locator.dart';
import 'package:flutter_crud/presentation/sections/movie_main_content_section.dart';
import 'package:flutter_crud/presentation/stores/del_btn_store.dart';
import 'package:flutter_crud/router/app_router.gr.dart';

@RoutePage()
class MovieMainScreen extends StatelessWidget {
  const MovieMainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final DelBtnStore delBtnStore = getIt.get<DelBtnStore>();
    var uniqueKey = ValueKey(DateTime.now().millisecondsSinceEpoch);
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        title: const Text("Movies Collection"),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: MovieMainContentSection(key: uniqueKey),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          delBtnStore.hideDelBtn();
          AutoRouter.of(context).push(MovieCrudRoute());
        },
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
        child: const Icon(Icons.add),
      ),
    );
  }
}
