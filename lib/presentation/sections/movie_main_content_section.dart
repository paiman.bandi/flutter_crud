import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud/core/service_locator.dart';
import 'package:flutter_crud/data/movie_data.dart';
import 'package:flutter_crud/presentation/stores/del_btn_store.dart';
import 'package:flutter_crud/presentation/stores/movie_store.dart';
import 'package:flutter_crud/presentation/stores/search_store.dart';
import 'package:flutter_crud/router/app_router.gr.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

class MovieMainContentSection extends StatelessWidget {
  MovieMainContentSection({super.key});

  final TextEditingController searchController = TextEditingController();

  Widget _wCard(BuildContext context, Movie movie) {
    final DelBtnStore delBtnStore = getIt.get<DelBtnStore>();
    return Card(
      elevation: 3,
      margin: const EdgeInsets.all(10),
      child: ListTile(
        onTap: () {
          delBtnStore.showDelBtn();
          AutoRouter.of(context).push(MovieCrudRoute(movieId: movie.id));
        },
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              movie.title,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              textAlign: TextAlign.left,
            ),
            Text(
              movie.director,
              style: const TextStyle(fontSize: 16),
              textAlign: TextAlign.left,
            ),
            const SizedBox(
              height: 20.0,
            ),
            Text(
              movie.genres
                  .toString()
                  .replaceAll('[', '')
                  .replaceAll(']', '')
                  .replaceAll(',', ' /'),
              style: const TextStyle(fontSize: 16),
              textAlign: TextAlign.right,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final MovieStore movieStore = getIt.get<MovieStore>();
    final SearchStore searchStore = getIt.get<SearchStore>();

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextField(
              controller: searchController,
              onChanged: (title) {
                if (title.isEmpty) {
                  searchStore.clearMovies();
                  searchStore.setIsSearch(false);
                } else {
                  searchStore.setIsSearch(true);
                  ObservableList<Movie> dataSearch =
                      movieStore.searchMoviesByTitle(title);

                  searchStore.setMovies(dataSearch);
                }
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Search by title...',
              ),
            ),
          ),
          Expanded(
              child: Observer(
                  builder: (_) => ListView.builder(
                      itemCount: searchStore.isSearch
                          ? searchStore.moviesSearch.length
                          : movieStore.movies.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _wCard(
                            context,
                            searchStore.isSearch
                                ? searchStore.moviesSearch[index]
                                : movieStore.movies[index]);
                      }))),
        ]);
  }
}
