// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'del_btn_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$DelBtnStore on _DelBtnStore, Store {
  late final _$isDelBtnShownAtom =
      Atom(name: '_DelBtnStore.isDelBtnShown', context: context);

  @override
  bool get isDelBtnShown {
    _$isDelBtnShownAtom.reportRead();
    return super.isDelBtnShown;
  }

  @override
  set isDelBtnShown(bool value) {
    _$isDelBtnShownAtom.reportWrite(value, super.isDelBtnShown, () {
      super.isDelBtnShown = value;
    });
  }

  late final _$_DelBtnStoreActionController =
      ActionController(name: '_DelBtnStore', context: context);

  @override
  void showDelBtn() {
    final _$actionInfo = _$_DelBtnStoreActionController.startAction(
        name: '_DelBtnStore.showDelBtn');
    try {
      return super.showDelBtn();
    } finally {
      _$_DelBtnStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void hideDelBtn() {
    final _$actionInfo = _$_DelBtnStoreActionController.startAction(
        name: '_DelBtnStore.hideDelBtn');
    try {
      return super.hideDelBtn();
    } finally {
      _$_DelBtnStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isDelBtnShown: ${isDelBtnShown}
    ''';
  }
}
