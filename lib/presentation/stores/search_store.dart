import 'package:flutter/material.dart';
import 'package:flutter_crud/data/movie_data.dart';
import 'package:mobx/mobx.dart';

part 'search_store.g.dart';

class SearchStore = _SearchStore with _$SearchStore;

abstract class _SearchStore with Store {
  _SearchStore() {
    _setupReactions();
  }

  @observable
  ObservableList<Movie> moviesSearch = ObservableList<Movie>();

  @observable
  bool isSearch = false;

  Movie findMovieById(String id) {
    return moviesSearch.firstWhere((movie) => movie.id == id);
  }

  @action
  void clearMovies() {
    moviesSearch.clear();
  }

  @action
  void setMovies(Iterable<Movie> movies) {
    moviesSearch.clear();
    moviesSearch.addAll(movies);
  }

  @action
  void setIsSearch(bool value) {
    isSearch = value;
  }

  void _setupReactions() {
    reaction((_) => moviesSearch, (ObservableList<Movie> movies) {
      debugPrint('Movies Search Updated: $movies');
    });

    reaction((_) => isSearch, (bool isSearch) {
      debugPrint('Is Search Updated: $isSearch');
    });
  }
}
