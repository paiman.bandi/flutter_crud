// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SearchStore on _SearchStore, Store {
  late final _$moviesSearchAtom =
      Atom(name: '_SearchStore.moviesSearch', context: context);

  @override
  ObservableList<Movie> get moviesSearch {
    _$moviesSearchAtom.reportRead();
    return super.moviesSearch;
  }

  @override
  set moviesSearch(ObservableList<Movie> value) {
    _$moviesSearchAtom.reportWrite(value, super.moviesSearch, () {
      super.moviesSearch = value;
    });
  }

  late final _$isSearchAtom =
      Atom(name: '_SearchStore.isSearch', context: context);

  @override
  bool get isSearch {
    _$isSearchAtom.reportRead();
    return super.isSearch;
  }

  @override
  set isSearch(bool value) {
    _$isSearchAtom.reportWrite(value, super.isSearch, () {
      super.isSearch = value;
    });
  }

  late final _$_SearchStoreActionController =
      ActionController(name: '_SearchStore', context: context);

  @override
  void clearMovies() {
    final _$actionInfo = _$_SearchStoreActionController.startAction(
        name: '_SearchStore.clearMovies');
    try {
      return super.clearMovies();
    } finally {
      _$_SearchStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setMovies(Iterable<Movie> movies) {
    final _$actionInfo = _$_SearchStoreActionController.startAction(
        name: '_SearchStore.setMovies');
    try {
      return super.setMovies(movies);
    } finally {
      _$_SearchStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIsSearch(bool value) {
    final _$actionInfo = _$_SearchStoreActionController.startAction(
        name: '_SearchStore.setIsSearch');
    try {
      return super.setIsSearch(value);
    } finally {
      _$_SearchStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
moviesSearch: ${moviesSearch},
isSearch: ${isSearch}
    ''';
  }
}
