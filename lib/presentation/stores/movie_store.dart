import 'package:flutter_crud/data/movie_data.dart';
import 'package:mobx/mobx.dart';

part 'movie_store.g.dart';

class MovieStore = _MovieStore with _$MovieStore;

abstract class _MovieStore with Store {
  @observable
  ObservableList<Movie> movies = ObservableList<Movie>();

  @observable
  ObservableList<String> selectedGenres = ObservableList<String>();

  @observable
  bool isUpdatedGenre = false;

  @action
  void setIsUpdatedGenre(bool value) {
    isUpdatedGenre = value;
  }

  @action
  void addGenre(String genre) {
    selectedGenres.add(genre);
  }

  @action
  void removeGenre(String genre) {
    selectedGenres.remove(genre);
  }

  @action
  void clearSelectedGenres() {
    selectedGenres.clear();
  }

  @action
  void addMovie(Movie movie) {
    movies.add(movie);
  }

  @action
  void updateMovie(String id, Movie updatedMovie) {
    final index = movies.indexWhere((movie) => movie.id == id);
    if (index != -1) {
      movies[index] = updatedMovie;
    }
  }

  @action
  void deleteMovie(String id) {
    movies.removeWhere((movie) => movie.id == id);
  }

  Movie? findMovieById(String id) {
    return movies.firstWhere(
      (movie) => movie.id == id,
      orElse: () => Movie(
          title: '',
          director: '',
          summary: '',
          genres: []), // null is allowed because the return type is Movie?
    );
  }

  @action
  ObservableList<Movie> searchMoviesByTitle(String title) {
    return ObservableList.of(movies.where(
        (movie) => movie.title.toLowerCase().contains(title.toLowerCase())));
  }
}
