import 'package:mobx/mobx.dart';

part 'del_btn_store.g.dart';

class DelBtnStore = _DelBtnStore with _$DelBtnStore;

abstract class _DelBtnStore with Store {
  @observable
  bool isDelBtnShown = false;

  @action
  void showDelBtn() {
    isDelBtnShown = true;
  }

  @action
  void hideDelBtn() {
    isDelBtnShown = false;
  }
}
