// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieStore on _MovieStore, Store {
  late final _$moviesAtom = Atom(name: '_MovieStore.movies', context: context);

  @override
  ObservableList<Movie> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(ObservableList<Movie> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  late final _$selectedGenresAtom =
      Atom(name: '_MovieStore.selectedGenres', context: context);

  @override
  ObservableList<String> get selectedGenres {
    _$selectedGenresAtom.reportRead();
    return super.selectedGenres;
  }

  @override
  set selectedGenres(ObservableList<String> value) {
    _$selectedGenresAtom.reportWrite(value, super.selectedGenres, () {
      super.selectedGenres = value;
    });
  }

  late final _$isUpdatedGenreAtom =
      Atom(name: '_MovieStore.isUpdatedGenre', context: context);

  @override
  bool get isUpdatedGenre {
    _$isUpdatedGenreAtom.reportRead();
    return super.isUpdatedGenre;
  }

  @override
  set isUpdatedGenre(bool value) {
    _$isUpdatedGenreAtom.reportWrite(value, super.isUpdatedGenre, () {
      super.isUpdatedGenre = value;
    });
  }

  late final _$_MovieStoreActionController =
      ActionController(name: '_MovieStore', context: context);

  @override
  void setIsUpdatedGenre(bool value) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.setIsUpdatedGenre');
    try {
      return super.setIsUpdatedGenre(value);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addGenre(String genre) {
    final _$actionInfo =
        _$_MovieStoreActionController.startAction(name: '_MovieStore.addGenre');
    try {
      return super.addGenre(genre);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeGenre(String genre) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.removeGenre');
    try {
      return super.removeGenre(genre);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearSelectedGenres() {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.clearSelectedGenres');
    try {
      return super.clearSelectedGenres();
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addMovie(Movie movie) {
    final _$actionInfo =
        _$_MovieStoreActionController.startAction(name: '_MovieStore.addMovie');
    try {
      return super.addMovie(movie);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateMovie(String id, Movie updatedMovie) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.updateMovie');
    try {
      return super.updateMovie(id, updatedMovie);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie(String id) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.deleteMovie');
    try {
      return super.deleteMovie(id);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  ObservableList<Movie> searchMoviesByTitle(String title) {
    final _$actionInfo = _$_MovieStoreActionController.startAction(
        name: '_MovieStore.searchMoviesByTitle');
    try {
      return super.searchMoviesByTitle(title);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movies: ${movies},
selectedGenres: ${selectedGenres},
isUpdatedGenre: ${isUpdatedGenre}
    ''';
  }
}
