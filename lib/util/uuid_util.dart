import 'dart:math';

String generateUUID() {
  final random = Random();
  final segments = List.generate(4, (index) {
    final value = random.nextInt(2 ^ 32);
    return value.toRadixString(16).padLeft(8, '0');
  });
  return '${segments[0]}-${segments[1]}-${segments[2]}-${segments[3]}';
}
