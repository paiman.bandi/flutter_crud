import 'package:flutter/material.dart';
import 'package:flutter_crud/core/service_locator.dart';
import 'package:flutter_crud/router/app_router.dart';

void main() {
  serviceLocatorInit();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final route = getIt.get<AppRouter>();
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      routerConfig: route.config(),
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
    );
  }
}
