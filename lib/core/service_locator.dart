import 'package:flutter_crud/presentation/stores/del_btn_store.dart';
import 'package:flutter_crud/presentation/stores/movie_store.dart';
import 'package:flutter_crud/presentation/stores/search_store.dart';
import 'package:flutter_crud/router/app_router.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void serviceLocatorInit() {
  getIt.registerFactory(() => AppRouter());
  getIt.registerLazySingleton(() => DelBtnStore());
  getIt.registerLazySingleton(() => MovieStore());
  getIt.registerLazySingleton(() => SearchStore());
}
