class Movie {
  String? id;
  String title;
  String director;
  String summary;
  List<String> genres;

  Movie({
    this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });
}
