# flutter_crud

A simple CRUD application for managing a movie collection, developed using Flutter and incorporating mobx, get_it, and auto_route for enhanced functionality.

```
flutter run
```
